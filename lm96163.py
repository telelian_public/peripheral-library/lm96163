from enum import Enum
from loguru import logger
from smbus2 import SMBus

DEFAULT_FANADDR = 0x4c

class LM96163Reg(int, Enum):
    LOCAL_TEMP = 0x0
    RMT_TEMP_MSB = 0x1
    ALERT_STATUS = 0x2
    CONFIG = 0x3
    CONVERSION_RATE = 0x4
    LOCAL_HIGH_SETPOINT = 0x5
    RMT_HIGH_SETPOINT_MSB = 0x7
    RMT_LOW_SETPOINT_MSB = 0x8
    ONE_SHOT = 0xf
    RMT_TEMP_LSB = 0x10
    RMT_TEMP_OFFSET_MSB = 0x11
    RMT_TEMP_OFFSET_LSB = 0x12
    RMT_HIGH_SETPOINT_LSB = 0x13
    RMT_LOW_SETPOINT_LSB = 0x14
    ALERT_MASK = 0x15
    RMT_T_CRIT_SETPOINT = 0x19
    RMT_T_CRIT_HYSTERESIS= 0x21
    REMOTE_DIODE_TRUTHERM_ENABLE = 0x30
    RMT_TEMP_U_S_LSB = 0x31
    RMT_TEMP_U_S_MSB = 0x32
    POR_STATUS = 0x33
    ENHANCED_CONFIG = 0x45
    TACH_COUNT_LSB = 0x46
    TACH_COUNT_MSB = 0x47
    TACH_LIMIT_LSB = 0x48
    TACH_LIMIT_MSB = 0x49
    PWM_RPM_CONFIG = 0x4a
    FAN_SPINUP_CONFIG = 0x4b
    PWM_VALUE = 0x4c
    PWM_FREQ = 0x4d
    LOOKUP_TABLE_TEMP_OFFSET = 0x4e
    LOOKUP_TABLE_TEMP_HYSTERESIS = 0x4f
    
    LOOKUP_TABLE_0 = 0x50
    """
    ~~~ lookup tables
    """
    LOOKUP_TABLE_23 = 0x67
    
    RMT_DIODE_TEMP_FILTER = 0xbf
    MANUFANTURE_ID = 0xfe
    REV_ID = 0xff
    
    

class LM96163(SMBus):
    def __init__(self, bus_number, i2c_addr=DEFAULT_FANADDR):
        super().__init__(bus=bus_number)
        self.i2c_addr = i2c_addr
        # tach enable
        self.writeReg(LM96163Reg.CONFIG, 0x4)
        
        # 1 After power up check to make sure that the Not Ready bit is cleared in the POR Status register [33] bit 7.
        while self.readReg(LM96163Reg.POR_STATUS) is not 0:
            pass
        
        # 2 Enable or disable Remote Diode TruTherm mode, [30] bit 1.
        self.writeReg(LM96163Reg.REMOTE_DIODE_TRUTHERM_ENABLE, 0x0)
            
        # 3 [4A] Write bits 0 and 1; 3 and 4. This includes tach settings if used, PWM internal clock select (1.4 kHz or 360 kHz) and PWM Output Polarity.
        reg = 1<<5 | 0<<4 | 0<<3 | 0
        self.writeReg(LM96163Reg.PWM_RPM_CONFIG, reg)
        
        # 4 [4B] Write bits 0 through 5 to program the spin-up settings.
        reg = 0<<5 | 1<<3 | 7
        self.writeReg(LM96163Reg.FAN_SPINUP_CONFIG, reg)
        
        # 5 [4D] Write bits 0 through 4 to set the frequency settings. This works with the PWM internal clock select. 
        # If 22.5 kHz is selected then enhanced fan control functions such as Lookup Table transition smoothing 
        # with extended PWM duty cycle resolution is available and should be setup [45].
        self.writeReg(LM96163Reg.PWM_FREQ, 8)

        # 6. Choose, then write, only one of the following:
        # A. [4F–67] the Lookup Table and [4E] the Lookup Table Offset, [45] Lookup Table Temperature Resolution can also be modified
        # self.writeReg(LM96163Reg.LOOKUP_TABLE_TEMP_OFFSET, 0)
        # self.writeReg(LM96163Reg.LOOKUP_TABLE_0, 5)
        # or
        # B. [4C] the PWM value bits 0 through 5 or bits 0 through 7 if extended duty cycle resolution is selected.
        self.writeReg(LM96163Reg.PWM_VALUE, 8)
        
        # 7 If Step 4A, Lookup Table, was chosen and written then write [4A] bit 5 PWPGM = 0. PWPGM should be set to 1 to enable writing to the fan control registers listed in this table.
        # tachlimit_lsb = self.readReg(LM96163Reg.TACH_LIMIT_LSB)
        # tachlimit = self.readReg(LM96163Reg.TACH_LIMIT_MSB)<<6 | tachlimit_lsb>>2
        # tachlimit = 0x3fff
        # self.writeReg(LM96163Reg.TACH_LIMIT_MSB, tachlimit>>6)
        # self.writeReg(LM96163Reg.TACH_LIMIT_LSB, (tachlimit&0xff) <<2)
        
        # tachlimit_lsb = self.readReg(LM96163Reg.TACH_LIMIT_LSB)
        # tachlimit = self.readReg(LM96163Reg.TACH_LIMIT_MSB)<<6 | tachlimit_lsb>>2
        
    def readReg(self, reg):
        return self.read_byte_data(self.i2c_addr, reg)
    
    def writeReg(self, reg, value):
        self.write_byte_data(self.i2c_addr, reg, value)
        
    def setDuty(self, duty_percent):
        freq = self.readReg(LM96163Reg.PWM_FREQ) *2
        duty = int(freq * duty_percent / 100)
        self.writeReg(LM96163Reg.PWM_VALUE, duty)
    
    def getRPM(self):
        tach_lsb = int(self.readReg(LM96163Reg.TACH_COUNT_LSB))
        tach_msb = int(self.readReg(LM96163Reg.TACH_COUNT_MSB))
        tach_cnt = tach_lsb | ((tach_msb<<8)&0xff00)
        if tach_cnt == 0xffff or tach_cnt == 0:
            return 0
        f = 1
        tach_freq = 5400000
        rpm = f * tach_freq / tach_cnt
        
        return rpm
    
